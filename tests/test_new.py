#!/usr/bin/env python3
"""Tests for imkin.py, package must be installed

>>> film = imkin.new('https://www.imdb.com/title/tt0068646/')
>>> print(film)
The Godfather (Крёстный отец, 1972, 175, 16+, https://www.imdb.com/title/tt0068646/)
>>> film = imkin.new('https://www.kinopoisk.ru/film/42664/')
>>> print(film.title)
Иван Васильевич меняет профессию
>>> print(film.year)
1973
>>> print(film.time)
88
>>> print(film.age)
6+
>>> print(film.alternate)
None
>>> print(film.url)
https://www.kinopoisk.ru/film/42664/
>>> time.sleep(4)
>>> film = imkin.new('https://www.imdb.com/title/tt2356777/')
>>> print(film.title)
True Detective
>>> print(film.alternate)
Настоящий детектив
>>> time.sleep(4)
>>> film.fetch_titles()
True
>>> print(film.amount_seasons())
3
>>> print(film.amount_episodes())
24
>>> print(film.amount_episodes(1))
8
>>> print(film.s(1).ep(1))
The Long Bright Dark
>>> time.sleep(7)
>>> film = imkin.new('https://www.kinopoisk.ru/film/681831/')
>>> print(film.title)
Настоящий детектив
>>> print(film.alternate)
True Detective
>>> time.sleep(7)
>>> film.fetch_titles()
True
>>> print(film.s(1).ep(1))
Долгая яркая тьма/The Long Bright Dark
"""
import imkin
import time


if __name__ == "__main__":
    import doctest
    doctest.testmod()
